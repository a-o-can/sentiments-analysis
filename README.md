# sentiment_analysis

GitLab Repo for "Python for Engineering Data Analysis - from Machine Learning to Visualization"- final project.

https://drive.google.com/drive/folders/1eXx0lUHtUzEWjBLlsJwznkqQ7FeGks1_?usp=sharing

Sentiment analysis or opinion mining is the computational study of opinions, sentiments, and emotions expressed in text[1]. This has caught our attention and we wanted to dive deeper in the topic. For this project we are using Python. 

Our research question is as follow: 
To what extent do stop words contain valuable information with respect to sentiment analysis in different machine learning algorithms? 

Our data set:
The IMDB dataset has been used [2]. The official description of the set is as follows:
We constructed a collection of 50,000 reviews from IMDB, allowing no more than 30 reviews per movie. The constructed dataset contains an even number of positive and negative reviews, so randomly guessing yields 50% accuracy.








[1]: X. Wang, W. Jiang, and Z. Luo, “Combination of convolutional and recurrent neural network for sentiment analysis of short texts,” in Proceedings of COLING 2016, the 26th international conference on computational linguistics: Technical papers, 2016, pp. 2428–2437.
[2]: A. L. Maas, R. E. Daly, P. T. Pham, D. Huang, A. Y. Ng, and C. Potts, “Learning word vectors for sentiment analysis,” in Proceedings of the 49th Annual Meeting of the Association for Computational Linguistics: Human Language Technologies, Portland, Oregon, USA: Association for Computational Linguistics, Jun. 2011, pp. 142–150. [Online]. Available: http://www.aclweb.org/anthology/P11-1015.