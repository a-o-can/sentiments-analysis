# Sentiment Analysis of IMDB Dataset
# Contributers: Ali Oguz Can, Marco Mair
# for Python for Engineering Data Analysis from Machine Learning to Visualization
# 2 ML models are applied, combined 12 csv are saved during runtime 
# 

# Import libraries
import os
import re
import time

import matplotlib.pyplot as plt
import pandas as pd

from bs4 import BeautifulSoup
import nltk

nltk.download('stopwords')
nltk.download('wordnet')

from nltk.corpus import stopwords

from nltk.stem import WordNetLemmatizer
from nltk.stem.porter import PorterStemmer

from sklearn.svm import LinearSVC
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score,  plot_confusion_matrix

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

from sklearn.model_selection import train_test_split

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, LSTM, Embedding, Masking, Bidirectional
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

# Used to get the current location of the script
tic_entire = time.perf_counter()

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
print(__location__)

#Enable or disable Graphiccard usage
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'


# --------------------------------------
# Definiton of various functions
def generate_csvs():
    # Build CSVs out of the seperate
    tic = time.perf_counter()

    # building the filepaths the txt files
    testset_pos = __location__ + "\\imdb_database\\test\\pos"
    testset_neg = __location__ + "\\imdb_database\\test\\neg"

    trainset_pos = __location__ + "\\imdb_database\\train\\pos"
    trainset_neg = __location__ + "\\imdb_database\\train\\neg"

    # Open txt files and write them as csv file
    for name in [testset_pos, testset_neg, trainset_pos, trainset_neg]:
        # splitting pathnames to get test,train,pos,neg
        split_paths = name.split('\\')
        # needed to clear it before appending again
        open(__location__ + '\\' + split_paths[-2] + '_' + split_paths[-1] + '.csv', 'w').close()

        with open(__location__ + '\\' + split_paths[-2] + '_' + split_paths[-1] + '.csv', mode='w') as writefile:
            writefile.write(', review, sentiment\n')

        for root, dirs, files in os.walk(name):
            for file in files:
                if file.endswith(".txt"):
                    filename = os.path.splitext(os.path.basename(file))[0]  # to get the filename without the .txt

                    split_filename = filename.split("_", 1)

                    entry_number = split_filename[0]

                    # print(entry_number)

                    with open(name + "/" + file, encoding='utf-8', errors='replace') as readfile:
                        file_input = readfile.readline()

                    # Strip ',' and "" out of the text.
                    file_input = re.sub('[",]', '', file_input)

                    with open(__location__ + '\\' + split_paths[-2] + '_' + split_paths[-1] + '.csv', encoding='utf-8',
                              errors='replace', mode='a') as writefile:
                        if (name == testset_pos or name == trainset_pos):
                            writefile.write(entry_number + ',' + file_input + ',' + 'positive' + '\n')
                        else:
                            writefile.write(entry_number + ',' + file_input + ',' + 'negative' + '\n')
        print('Loop finished for: ' + name)

    toc = time.perf_counter()
    print(f"Building CSV's took {toc - tic:0.4f} seconds.\n")


# get csv data and write it into pandas framework | Input: string with the csv name
def csv_to_pandas(name_of_csv):
    tic = time.perf_counter()

    # read csvs
    temp_dataframe = pd.read_csv(__location__ + "\\" + name_of_csv + ".csv", sep=',', engine='python', encoding='utf-8',
                                 index_col=0)

    # sort index of dataframes
    temp_dataframe = temp_dataframe.sort_index()

    toc = time.perf_counter()
    print(f"CSV to Pandas took {toc - tic:0.4f} seconds.")
    return temp_dataframe


# Clean CSV data from everything | Input: pandas dataframe, boolean if stopwords should be cleaned or not, put in stemming of lemmatization
def clean_reviews(dataframe, delete_stopwords, use_stem_or_lem):
    tic = time.perf_counter()

    # init cleaned dataframe with input dataframe and then rewrite every review
    cleaned_dataframe = dataframe

    for i in range(dataframe.shape[0]):
        # for i in range(0,100):
        # Remove HTML content
        soup = BeautifulSoup(dataframe.iloc[i, 0], "html.parser")
        review = soup.get_text()

        # Remove ,!? and other signs/remove everything except upper and lower case letters
        review = re.sub('\[[^]]*\]', ' ', review)
        review = re.sub('[^a-zA-Z]', ' ', review)
        # Lower all characters
        review = review.lower()
        # replace spaces with single space
        review = re.sub('\s+', ' ', review)

        # removing stop words
        review = review.split()
        if(delete_stopwords == True):
            review = [word for word in review if word not in set(stopwords.words('english'))]

        # Stemming/Lemmatization with choice
        if (use_stem_or_lem == 'stem'):
            ps = PorterStemmer()
            review = [ps.stem(word) for word in review]
        elif (use_stem_or_lem == 'lem'):
            lem = WordNetLemmatizer()
            review = [lem.lemmatize(word) for word in review]
        elif (use_stem_or_lem == 'none'):
            print('No stemming or lemmatizing.')
        else:
            print('Wrong keywords, use "stem","lem" or "none"!')

        review = ' '.join(review)

        cleaned_dataframe.iloc[i, 0] = review

    toc = time.perf_counter()
    print(f"Cleaning reviews from special characters, moving from upper to lower case, etc. took {toc - tic:0.4f} seconds.\n")
    return cleaned_dataframe


def write_cleaned_csvs(dataframe, name):
    dataframe.to_csv(__location__ + '\\' + name + '.csv', index=True)


# --------------------------------------
# Main Programm
#
# If you just want to try the models, start commenting out here.
# """
generate_csvs()

# use function to get data from csvs
data_test_neg1 = csv_to_pandas(name_of_csv="test_neg")
data_test_neg2 = csv_to_pandas(name_of_csv="test_neg")

data_test_pos1 = csv_to_pandas(name_of_csv="test_pos")
data_test_pos2 = csv_to_pandas(name_of_csv="test_pos")

data_train_neg1 = csv_to_pandas(name_of_csv="train_neg")
data_train_neg2 = csv_to_pandas(name_of_csv="train_neg")

data_train_pos1 = csv_to_pandas(name_of_csv="train_pos")
data_train_pos2 = csv_to_pandas(name_of_csv="train_pos")


# use function to clean dataframes without stopwords
data_test_neg_without_stopwords = clean_reviews(dataframe=data_test_neg1, delete_stopwords=True, use_stem_or_lem='lem')
data_test_pos_without_stopwords = clean_reviews(dataframe=data_test_pos1, delete_stopwords=True, use_stem_or_lem='lem')
data_train_neg_without_stopwords = clean_reviews(dataframe=data_train_neg1, delete_stopwords=True, use_stem_or_lem='lem')
data_train_pos_without_stopwords = clean_reviews(dataframe=data_train_pos1, delete_stopwords=True, use_stem_or_lem='lem')

# use function to clean dataframes with stopwords
data_test_neg_with_stopwords = clean_reviews(dataframe=data_test_neg2, delete_stopwords=False, use_stem_or_lem='lem')
data_test_pos_with_stopwords = clean_reviews(dataframe=data_test_pos2, delete_stopwords=False, use_stem_or_lem='lem')
data_train_neg_with_stopwords = clean_reviews(dataframe=data_train_neg2, delete_stopwords=False, use_stem_or_lem='lem')
data_train_pos_with_stopwords = clean_reviews(dataframe=data_train_pos2, delete_stopwords=False, use_stem_or_lem='lem')

# write to csv again without stopwords
write_cleaned_csvs(dataframe=data_test_neg_without_stopwords, name='cleaned_test_neg_without_stopwords')
write_cleaned_csvs(dataframe=data_test_pos_without_stopwords, name='cleaned_test_pos_without_stopwords')
write_cleaned_csvs(dataframe=data_train_neg_without_stopwords, name='cleaned_train_neg_without_stopwords')
write_cleaned_csvs(dataframe=data_train_pos_without_stopwords, name='cleaned_train_pos_without_stopwords')

# write to csv again with stopwords
write_cleaned_csvs(dataframe=data_test_neg_with_stopwords, name='cleaned_test_neg_with_stopwords')
write_cleaned_csvs(dataframe=data_test_pos_with_stopwords, name='cleaned_test_pos_with_stopwords')
write_cleaned_csvs(dataframe=data_train_neg_with_stopwords, name='cleaned_train_neg_with_stopwords')
write_cleaned_csvs(dataframe=data_train_pos_with_stopwords, name='cleaned_train_pos_with_stopwords')

# If you just want to try the models, stop commenting out here.
# """
tic_fitting = time.perf_counter()

# get data from cleaned csvs without stopwords
cleaned_test_neg_without_stopwords = pd.read_csv(__location__ + '\\cleaned_test_neg_without_stopwords.csv', index_col=0)
cleaned_test_pos_without_stopwords = pd.read_csv(__location__ + '\\cleaned_test_pos_without_stopwords.csv', index_col=0)
cleaned_train_neg_without_stopwords = pd.read_csv(__location__ + '\\cleaned_train_neg_without_stopwords.csv', index_col=0)
cleaned_train_pos_without_stopwords = pd.read_csv(__location__ + '\\cleaned_train_pos_without_stopwords.csv', index_col=0)

# get data from cleaned csvs with stopwords
cleaned_test_neg_with_stopwords = pd.read_csv(__location__ + '\\cleaned_test_neg_with_stopwords.csv', index_col=0)
cleaned_test_pos_with_stopwords = pd.read_csv(__location__ + '\\cleaned_test_pos_with_stopwords.csv', index_col=0)
cleaned_train_neg_with_stopwords = pd.read_csv(__location__ + '\\cleaned_train_neg_with_stopwords.csv', index_col=0)
cleaned_train_pos_with_stopwords = pd.read_csv(__location__ + '\\cleaned_train_pos_with_stopwords.csv', index_col=0)

# combine both datasets to do train/test split afterwards without stopwords
testdata_without_stopwords = pd.concat([cleaned_test_neg_without_stopwords, cleaned_test_pos_without_stopwords], ignore_index=True)
traindata_without_stopwords = pd.concat([cleaned_train_neg_without_stopwords, cleaned_train_pos_without_stopwords], ignore_index=True)

# combine both datasets to do train/test split afterwards with stopwords
testdata_with_stopwords = pd.concat([cleaned_test_neg_with_stopwords, cleaned_test_pos_with_stopwords], ignore_index=True)
traindata_with_stopwords = pd.concat([cleaned_train_neg_with_stopwords, cleaned_train_pos_with_stopwords], ignore_index=True)

dataset = pd.concat([testdata_without_stopwords, traindata_without_stopwords], ignore_index=True)
dataset_w = pd.concat([testdata_with_stopwords, traindata_with_stopwords], ignore_index=True)
#_w == with stopwords

# convert sentiment strings to numbers (without/with)
dataset.iloc[:, 1] = (dataset.iloc[:, 1].replace({'positive': 1, 'negative': 0})).values

dataset_w.iloc[:, 1] = (dataset.iloc[:, 1].replace({'positive': 1, 'negative': 0})).values

# Split whole dataset to be able to get different testsizes (without/with)
dataset_train, dataset_test, dataset_train_label, dataset_test_label = train_test_split(dataset.iloc[:, 0],
                                                                                        dataset.iloc[:, 1],
                                                                                        test_size=0.25, random_state=42)

dataset_train_w, dataset_test_w, dataset_train_label_w, dataset_test_label_w = train_test_split(dataset_w.iloc[:, 0],
                                                                                                dataset_w.iloc[:, 1],
                                                                                                test_size=0.25,
                                                                                                random_state=42)

# ----------------------------------------------------------------------------------------------------------------------------------
# ML - SVM

#TF-IDF vectorization
tfidf_vec = TfidfVectorizer(ngram_range=(1, 3))

tfidf_vec_train = tfidf_vec.fit_transform(dataset_train)
tfidf_vec_test = tfidf_vec.transform(dataset_test)

tfidf_vec_train_w = tfidf_vec.fit_transform(dataset_train_w)
tfidf_vec_test_w = tfidf_vec.transform(dataset_test_w)

#LinearSVM with tf-idf, stopwords removal and lemmatization
linear_svc_tfidf = LinearSVC(C=1, random_state=42)
linear_svc_tfidf.fit(tfidf_vec_train, dataset_train_label)

predict_svc_tfidf = linear_svc_tfidf.predict(tfidf_vec_test)

#Ausgabe Classification report for LinearSVM with tf-idf, stopwords removal and lemmatization
print("Classification Report: \n", classification_report(dataset_test_label, predict_svc_tfidf, target_names=['Negative','Positive']))
print("Confusion Matrix: \n", confusion_matrix(dataset_test_label, predict_svc_tfidf))
accuracy = accuracy_score(dataset_test_label, predict_svc_tfidf)
print("Accuracy: \n", accuracy)


#LinearSVM with tf-idf, with stopwords and lemmatization
linear_svc_tfidf_w = LinearSVC(C=1, random_state=42)
linear_svc_tfidf_w.fit(tfidf_vec_train_w, dataset_train_label_w)

predict_svc_tfidf_w = linear_svc_tfidf_w.predict(tfidf_vec_test_w)

#Ausgabe Classification report for LinearSVM with tf-idf, stopwords removal and lemmatization
print("Classification Report: \n", classification_report(dataset_test_label_w, predict_svc_tfidf_w, target_names=['Negative','Positive']))
print("Confusion Matrix: \n", confusion_matrix(dataset_test_label_w, predict_svc_tfidf_w))
accuracy_w = accuracy_score(dataset_test_label_w, predict_svc_tfidf_w)
print("Accuracy: \n", accuracy_w)


plot_confusion_matrix(linear_svc_tfidf, tfidf_vec_test, dataset_test_label, cmap=plt.cm.Blues, display_labels=[0, 1])
plt.title(f'Confusion Matrix without Stopwords - Accuracy: {accuracy:.3f}')
plt.show()

plot_confusion_matrix(linear_svc_tfidf_w, tfidf_vec_test_w, dataset_test_label_w, cmap=plt.cm.Blues, display_labels=[0, 1])
plt.title(f'Confusion Matrix with Stopwords- Accuracy: {accuracy_w:.3f}')
plt.show()



# ----------------------------------------------------------------------------------------------------------------------------------
# ML - Neural Networks
max_features = 20000
maxlen = 200
# the maximum number of words to keep, based on word frequency. Only the most common num_words-1 words will be kept.
tokenizer = Tokenizer(num_words=max_features)

# --------------------------
# make a data frame out of the training data set (without/with)
train = pd.DataFrame(dataset_train)
train.columns = ['review']

train_w = pd.DataFrame(dataset_train_w)
train_w.columns = ['review']
# make a data frame out of the testing data set (without/with)
test = pd.DataFrame(dataset_test)
test.columns = ['review']

test_w = pd.DataFrame(dataset_test_w)
test_w.columns = ['review']
# --------------------------

# Apply the tokenizer

# tf.keras.preprocessing.text.Tokenizer: This class allows to vectorize a text corpus, by turning each text into either
# a sequence of integers (each integer being the index of a token in a dictionary)
# or into a vector where the coefficient for each token could be binary, based on word count, based on tf-idf...

# Updates internal vocabulary based on a list of texts. (without/with)
tokenizer.fit_on_texts(train['review'])

tokenizer.fit_on_texts(train_w['review'])
# Transforms each text in texts to a sequence of integers. (without/with)
X_train_token = tokenizer.texts_to_sequences(train['review'])
tokenizer.fit_on_texts(test['review'])
X_test_token = tokenizer.texts_to_sequences(test['review'])

X_train_token_w = tokenizer.texts_to_sequences(train_w['review'])
tokenizer.fit_on_texts(test_w['review'])
X_test_token_w = tokenizer.texts_to_sequences(test_w['review'])

# --------------------------

# Apply the Padding : All the neural networks require to have inputs that have the same shape and size.

# Bringing token vectors to the same size (maxlen = 200) by adding padding (0's) at the end (post)
X_train = pad_sequences(X_train_token, maxlen=maxlen, padding='post')
X_test = pad_sequences(X_test_token, maxlen=maxlen, padding='post')
print(X_train.shape, X_test.shape)

y_train = dataset_train_label.copy()
y_test = dataset_test_label.copy()

X_train_w = pad_sequences(X_train_token_w, maxlen=maxlen, padding='post')
X_test_w = pad_sequences(X_test_token_w, maxlen=maxlen, padding='post')
print(X_train_w.shape, X_test_w.shape)

y_train_w = dataset_train_label_w.copy()
y_test_w = dataset_test_label_w.copy()

# --------------------------

# Initialize the Model

# tf.keras.Sequential: Sequential groups a linear stack of layers into a tf.keras.Model.

# tf.keras.layers.Embedding: Turns positive integers (indexes) into dense vectors of fixed size.
# input_dim: Size of Vocabulary = max_features
# output_dim: Dimension of the dense embedding = 64
# mask_zero: whether or not the input value 0 is a special "padding" value that should be masked out. = False,
#   index 0 can be used in the vocabulary

# Bidirectional wrapper for RNNs.(Recurrent Neural Networks: are a class of neural networks that is powerful
# for modeling sequence data such as time series or natural language.)

# tf.keras.layers.LSTM: Long Short-Term Memory layer - Hochreiter 1997.
# units: dimensionality of the output space. = 64
# dropout: Fraction of the units to drop for the linear transformation of the inputs.

# tf.keras.layers.Dense: Just your regular densely-connected NN layer.
# unit: dimensionality of the output space. = 64
# activation: Activation function to use. = sigmoid
model = Sequential([
    Embedding(max_features, 64, mask_zero=True),
    Bidirectional(LSTM(64, dropout=0.2)),
    Dense(64, activation='sigmoid'),
    Dense(1),
])

# --------------------------

# Configure the model for training

# binary_crossentropy(loss): Computes the cross-entropy loss between true labels and predicted labels.
# adam(optimizer): Optimizer that implements the Adam algorithm.
# Accuracy(metrics): Calculates how often predictions equal labels.
model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

# --------------------------

# Train the model

# input data = X_train
# target data (labels): y_train
# batch_size:  Number of samples per batch of computation
# validation_data = Data on which to evaluate the loss and any model metrics at the end of each epoch.
# The model will not be trained on this data.

# (without/with)
history = model.fit(X_train, y_train,
                    batch_size=50,
                    epochs=3,
                    validation_data=(X_test, y_test))

history_w = model.fit(X_train_w, y_train_w,
                      batch_size=50,
                      epochs=3,
                      validation_data=(X_test_w, y_test_w))
# Performance history
history.history

history_w.history

# --------------------------
# Plotting the results (without/with)

plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'], '')
plt.xlabel("Epochs")
plt.ylabel('Accuracy')
plt.title('Change of Accuracy over Epochs without stopwords')
plt.legend(['accuracy', 'val_accuracy'])
plt.show()
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'], '')
plt.xlabel("Epochs")
plt.ylabel('Loss')
plt.title('Change of Loss over Epochs without stopwords')
plt.legend(['loss', 'val_loss'])
plt.show()

plt.plot(history_w.history['accuracy'])
plt.plot(history_w.history['val_accuracy'], '')
plt.xlabel("Epochs")
plt.ylabel('Accuracy')
plt.title('Change of Accuracy over Epochs with stopwords')
plt.legend(['accuracy', 'val_accuracy'])
plt.show()
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'], '')
plt.xlabel("Epochs")
plt.ylabel('Loss')
plt.title('Change of Loss over Epochs with stopwords')
plt.legend(['loss', 'val_loss'])
plt.show()


toc_entire = time.perf_counter()
toc_fitting = time.perf_counter()
print(f"Whole programm without plotting graphs took {toc_entire - tic_entire:0.4f} seconds.\n")
print(f"Fitting pr plotting graphs took {toc_fitting - tic_fitting:0.4f} seconds.\n")